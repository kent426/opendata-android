package a00961903.comp3717.bcit.ca.opendata;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;

public class CategoryContentProvider
    extends ContentProvider
{
    private static final UriMatcher uriMatcher;
    private static final int NWDATA_URI = 1;
    private static final int DATASET_URI =2;
    private static final int DATASET_DES_URI = 3;
    private CategoryOpenHelper categoryOpenHelper;
    public static final Uri CONTENT_URI;
    public static final Uri CATE_TO_DATASET_URI;
    public static  final Uri DATASET_TO_DES_URI;

    static
    {
        uriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
        uriMatcher.addURI("a00961903.comp3717.bcit.ca.opendata", "nwData", NWDATA_URI);
        uriMatcher.addURI("a00961903.comp3717.bcit.ca.opendata", "nwData/dataset", DATASET_URI);
        uriMatcher.addURI("a00961903.comp3717.bcit.ca.opendata", "nwData/dataset/datasetdescription", DATASET_DES_URI);
    }

    static
    {
        CONTENT_URI = Uri.parse("content://a00961903.comp3717.bcit.ca.opendata/nwData");
        CATE_TO_DATASET_URI = Uri.parse("content://a00961903.comp3717.bcit.ca.opendata/nwData/dataset");
        DATASET_TO_DES_URI = Uri.parse("content://a00961903.comp3717.bcit.ca.opendata/nwData/dataset/datasetdescription");
    }

    @Override
    public boolean onCreate()
    {
        categoryOpenHelper = CategoryOpenHelper.getInstance(getContext());

        return true;
    }

    @Override
    public Cursor query(final Uri uri,
                        final String[] projection,
                        final String selection,
                        final String[] selectionArgs,
                        final String sortOrder)
    {
        final Cursor cursor;

        switch (uriMatcher.match(uri))
        {
            case NWDATA_URI:
            {
                final SQLiteDatabase db;

                db     = categoryOpenHelper.getWritableDatabase();
                cursor = categoryOpenHelper.getAllCategory(getContext(), db);
                break;
            }
            case DATASET_URI:
            {
                final SQLiteDatabase db;

                db     = categoryOpenHelper.getWritableDatabase();
                cursor = categoryOpenHelper.getDataSet(getContext(),selectionArgs, db);
                break;
            }
            case DATASET_DES_URI:
            {
                final SQLiteDatabase db;
                db     = categoryOpenHelper.getWritableDatabase();
                cursor = categoryOpenHelper.getDataSetDescription(getContext(),selectionArgs, db);
                break;
            }
            default:
            {
                throw new IllegalArgumentException("Unsupported URI: " + uri);
            }
        }

        return (cursor);
    }

    @Override
    public String getType(final Uri uri)
    {
        final String type;

        switch(uriMatcher.match(uri))
        {
            case NWDATA_URI:
                type = "vnd.android.cursor.dir/vnd.a00961903.comp3717.bcit.ca.database.nwData";
                break;
            case DATASET_URI:
                type = "vnd.android.cursor.dir/vnd.a00961903.comp3717.bcit.ca.database.nwData.dataset.datasetItem";
                break;
            case DATASET_DES_URI:
                type = "vnd.android.cursor.dir/vnd.a00961903.comp3717.bcit.ca.database.nwData.dataset.datasetdescription";
                break;
            default:
                throw new IllegalArgumentException("Unsupported URI: " + uri);
        }

        return (type);
    }

    @Override
    public int delete(final Uri uri,
                      final String selection,
                      final String[] selectionArgs)
    {
        // Implement this to handle requests to delete one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public Uri insert(final Uri uri,
                      final ContentValues values)
    {
        // TODO: Implement this to handle requests to insert a new row.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public int update(final Uri uri,
                      final ContentValues values,
                      final String selection,
                      final String[]      selectionArgs)
    {
        // TODO: Implement this to handle requests to update one or more rows.
        throw new UnsupportedOperationException("Not yet implemented");
    }
}
