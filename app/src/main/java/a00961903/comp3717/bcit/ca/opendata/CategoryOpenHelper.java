package a00961903.comp3717.bcit.ca.opendata;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.DatabaseUtils;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

/**
 * Created by darcy on 2016-10-16.
 */

public final class CategoryOpenHelper
    extends SQLiteOpenHelper
{
    private static final String TAG = CategoryOpenHelper.class.getName();
    private static final int SCHEMA_VERSION = 1;
    private static final String DB_NAME = "nwData.db";

    private static final String CATEGORY_TABLE_NAME = "category";
    private static final String ID_COLUMN_NAME = "_id";
    private static final String CATEGORYITEM_COLUMN_NAME = "categoryItem";

    private static final String DATASET_TABLE_NAME = "dataset";
    private static final String ID_COLUMN_NAME_IN_DATASET = "_id";
    private static final String CATEGORYTYPE_COLUMN_IN_DATASET = "categoryType";
    private static final String DATASET_COLUMN_IN_DATASET = "datasetItem";
    private static final String DESDATASET_COLUMN_IN_DATASET = "datasetdescription";
    private static CategoryOpenHelper instance;

    private CategoryOpenHelper(final Context ctx)
    {
        super(ctx, DB_NAME, null, SCHEMA_VERSION);
    }

    public synchronized static CategoryOpenHelper getInstance(final Context context)
    {
        if(instance == null)
        {
            instance = new CategoryOpenHelper(context.getApplicationContext());
        }

        return instance;
    }

    @Override
    public void onConfigure(final SQLiteDatabase db)
    {
        super.onConfigure(db);

        setWriteAheadLoggingEnabled(true);
        db.setForeignKeyConstraintsEnabled(true);
    }

    @Override
    public void onCreate(final SQLiteDatabase db)
    {
        final String CREATE_CATEGORY_TABLE;
        final String CREATE_DATASET_TABLE;

        CREATE_CATEGORY_TABLE = "CREATE TABLE IF NOT EXISTS "  + CATEGORY_TABLE_NAME + " ( " +
                            ID_COLUMN_NAME   + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            CATEGORYITEM_COLUMN_NAME + " TEXT NOT NULL)";

        CREATE_DATASET_TABLE = "CREATE TABLE IF NOT EXISTS " + DATASET_TABLE_NAME + " ( " +
                            ID_COLUMN_NAME_IN_DATASET + " INTEGER PRIMARY KEY AUTOINCREMENT, " +
                            CATEGORYTYPE_COLUMN_IN_DATASET + " TEXT NOT NULL, " +
                            DATASET_COLUMN_IN_DATASET + " TEXT NOT NULL, "+
                            DESDATASET_COLUMN_IN_DATASET + " TEXT NOT NULL)";
                db.execSQL(CREATE_CATEGORY_TABLE);
                db.execSQL(CREATE_DATASET_TABLE);
    }

    @Override
    public void onUpgrade(final SQLiteDatabase db,
                          final int oldVersion,
                          final int newVersion)
    {
    }

    public long getNumberOfCategories(final SQLiteDatabase db)
    {
        final long numEntries;

        numEntries = DatabaseUtils.queryNumEntries(db, CATEGORY_TABLE_NAME);

        return (numEntries);
    }

    public void insertCategory(final SQLiteDatabase db,
                           final String name)
    {
        final ContentValues contentValues;

        contentValues = new ContentValues();
        contentValues.put(CATEGORYITEM_COLUMN_NAME, name);
        db.insert(CATEGORY_TABLE_NAME, null, contentValues);
    }

    public void insertDataSet(final SQLiteDatabase db,
                              final String categoryType,
                              final String dataset,
                              final String des)
    {
        final ContentValues contentValues;

        contentValues = new ContentValues();
        contentValues.put(CATEGORYTYPE_COLUMN_IN_DATASET, categoryType);
        contentValues.put(DATASET_COLUMN_IN_DATASET, dataset);
        contentValues.put(DESDATASET_COLUMN_IN_DATASET, des);
        db.insert(DATASET_TABLE_NAME, null, contentValues);
    }

    public int deleteCategory(final SQLiteDatabase db,
                          final String name)
    {
        final int rows;

        rows = db.delete(CATEGORY_TABLE_NAME,
                         CATEGORYITEM_COLUMN_NAME + " = ?",
                         new String[]
                         {
                                 "categoryItem",
                         });

        return (rows);
    }

    public Cursor getDataSet(final Context context,
                             final String[] category,
                             final SQLiteDatabase db)
    {
        final Cursor cursor;

        cursor = db.query(DATASET_TABLE_NAME,
                new String[]{ID_COLUMN_NAME_IN_DATASET,DATASET_COLUMN_IN_DATASET},
                "categoryType = ?",
                category,
                null,
                null,
                null,
                null);
        cursor.setNotificationUri(context.getContentResolver(), CategoryContentProvider.CATE_TO_DATASET_URI);

        return cursor;
    }

    public Cursor getDataSetDescription(final Context context,
                             final String[] datasetname,
                             final SQLiteDatabase db)
    {
        final Cursor cursor;

        cursor = db.query(DATASET_TABLE_NAME,
                new String[]{ID_COLUMN_NAME_IN_DATASET,DESDATASET_COLUMN_IN_DATASET},
                "datasetItem = ?",
                datasetname,
                null,
                null,
                null,
                null);
        cursor.setNotificationUri(context.getContentResolver(), CategoryContentProvider.DATASET_TO_DES_URI);

        return cursor;
    }

    public Cursor getAllCategory(final Context context,
                              final SQLiteDatabase db)
    {
        final Cursor cursor;

        cursor = db.query(CATEGORY_TABLE_NAME,
                          null,
                          null,     // selection, null = *
                          null,     // selection args (String[])
                          null,     // group by
                          null,     // having
                          null,     // order by
                          null);    // limit

        cursor.setNotificationUri(context.getContentResolver(), CategoryContentProvider.CONTENT_URI);

        return (cursor);
    }
}
