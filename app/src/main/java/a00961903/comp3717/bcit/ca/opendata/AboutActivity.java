package a00961903.comp3717.bcit.ca.opendata;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.Spanned;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;

public class AboutActivity extends AppCompatActivity {

    private CategoryOpenHelper categoryOpenHelper;
    private TextView tv;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final LoaderManager manager;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        String[] datasetname = getIntent().getExtras().getStringArray("dataset");
        getSupportActionBar().setTitle(datasetname[0]);

        manager = getLoaderManager();

        Bundle bd = getIntent().getExtras();
        manager.initLoader(0, bd, new AboutActivity.CategoryLoaderCallbacks());

        tv = (TextView)findViewById(R.id.textView);
        tv.setMovementMethod(new ScrollingMovementMethod());




    }

    private class CategoryLoaderCallbacks
            implements LoaderManager.LoaderCallbacks<Cursor>
    {
        @Override
        public Loader<Cursor> onCreateLoader(final int    id,
                                             final Bundle args)
        {
            final Uri uri;
            final CursorLoader loader;
            final String[] datasetname = args.getStringArray("dataset");

            uri    = CategoryContentProvider.DATASET_TO_DES_URI;
            loader = new CursorLoader(AboutActivity.this, uri, null, null, datasetname, null);

            return (loader);
        }

        @Override
        public void onLoadFinished(final Loader<Cursor> loader,
                                   final Cursor         data)
        {

            if (data.moveToFirst()){
                do{
                    String desc = data.getString(data.getColumnIndex("datasetdescription"));


                    Spanned htmlAsSpanned = Html.fromHtml(desc);
                    tv.setText(htmlAsSpanned);
                }while(data.moveToNext());
            }
            data.close();

        }

        @Override
        public void onLoaderReset(final Loader<Cursor> loader)
        {
            tv.setText("");
        }
    }
}
