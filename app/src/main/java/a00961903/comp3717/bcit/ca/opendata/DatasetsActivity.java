package a00961903.comp3717.bcit.ca.opendata;

import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class DatasetsActivity extends AppCompatActivity {

    private CategoryOpenHelper categoryOpenHelper;
    private SimpleCursorAdapter adapter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        final LoaderManager manager;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_datasets);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        String[] dataname = getIntent().getExtras().getStringArray("cate");
        getSupportActionBar().setTitle(dataname[0]);



        categoryOpenHelper = CategoryOpenHelper.getInstance(getApplicationContext());
        adapter = new SimpleCursorAdapter(getBaseContext(),
                android.R.layout.simple_list_item_1,
                null,
                new String[] {
                        "datasetItem",
                },
                new int[]{
                        android.R.id.text1,
                },
                0);


        ListView datasetLV = (ListView) findViewById(R.id.setList);

        datasetLV.setAdapter(adapter);
        manager = getLoaderManager();

        Bundle bd = getIntent().getExtras();
        manager.initLoader(0, bd, new DatasetsActivity.CategoryLoaderCallbacks());

        datasetLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> av, View view, int position, long id) {
                Intent toShow = new Intent(DatasetsActivity.this, AboutActivity.class);
                //get the Click item text
                String item = ((TextView)view.findViewById(android.R.id.text1)).getText().toString();
                Bundle bd = new Bundle();
                bd.putStringArray("dataset", new String[]{item});
                toShow.putExtras(bd);
                /*Toast t = Toast.makeText(getBaseContext(), item,Toast.LENGTH_SHORT);
                t.show();*/


                startActivity(toShow);

            }
        });

    }

    private class CategoryLoaderCallbacks
            implements LoaderManager.LoaderCallbacks<Cursor>
    {
        @Override
        public Loader<Cursor> onCreateLoader(final int    id,
                                             final Bundle args)
        {
            final Uri uri;
            final CursorLoader loader;
            final String[] cate = args.getStringArray("cate");

            uri    = CategoryContentProvider.CATE_TO_DATASET_URI;
            loader = new CursorLoader(DatasetsActivity.this, uri, null, null, cate, null);

            return (loader);
        }

        @Override
        public void onLoadFinished(final Loader<Cursor> loader,
                                   final Cursor         data)
        {
            adapter.swapCursor(data);
        }

        @Override
        public void onLoaderReset(final Loader<Cursor> loader)
        {
            adapter.swapCursor(null);
        }
    }
}
