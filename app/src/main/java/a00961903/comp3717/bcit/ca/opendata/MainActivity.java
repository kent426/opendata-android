package a00961903.comp3717.bcit.ca.opendata;

import android.app.Activity;
import android.app.ListActivity;
import android.app.LoaderManager;
import android.content.CursorLoader;
import android.content.Intent;
import android.content.Loader;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.SimpleCursorAdapter;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private CategoryOpenHelper categoryOpenHelper;
    private SimpleCursorAdapter adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        final LoaderManager manager;
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getSupportActionBar().setDisplayShowTitleEnabled(true);
        getSupportActionBar().setTitle("NewWest OpenData");

        categoryOpenHelper = CategoryOpenHelper.getInstance(getApplicationContext());
        adapter = new SimpleCursorAdapter(getBaseContext(),
                                            android.R.layout.simple_list_item_1,
                                            null,
                                            new String[] {
                                                    "categoryItem",
                                            },
                                            new int[]{
                                                    android.R.id.text1,
                                            },
                                             0);


        final ListView cateLV = (ListView) findViewById(R.id.categoryList);

        cateLV.setAdapter(adapter);
        manager = getLoaderManager();
        manager.initLoader(0, null, new CategoryLoaderCallbacks());
        init();

        cateLV.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> av, View view, int position, long id) {
                Intent toDataset = new Intent(MainActivity.this, DatasetsActivity.class);
                //get the Click item text
                String item = ((TextView)view.findViewById(android.R.id.text1)).getText().toString();
                Bundle bd = new Bundle();
                bd.putStringArray("cate", new String[]{item});
                toDataset.putExtras(bd);
                /*Toast t = Toast.makeText(getBaseContext(), item,Toast.LENGTH_SHORT);
                t.show();*/


                startActivity(toDataset);

            }
        });


    }

    private void init()
    {
        final SQLiteDatabase db;
        final long           numEntries;

        db = categoryOpenHelper.getWritableDatabase();
        numEntries = categoryOpenHelper.getNumberOfCategories(db);

        if(numEntries == 0)
        {
            db.beginTransaction();

            try
            {
                categoryOpenHelper.insertCategory(db,
                        "Business and Economy");
                categoryOpenHelper.insertCategory(db,
                        "City Government");
                categoryOpenHelper.insertCategory(db,
                        "Community");
                categoryOpenHelper.insertCategory(db,
                        "Electrical");
                categoryOpenHelper.insertCategory(db,
                        "Environment");
                categoryOpenHelper.insertCategory(db,
                        "Finance");
                categoryOpenHelper.insertCategory(db,
                        "Heritage");
                categoryOpenHelper.insertCategory(db,
                        "Lands and Development");
                categoryOpenHelper.insertCategory(db,
                        "Parks and Recreation");
                categoryOpenHelper.insertCategory(db,
                        "Public Safety");
                categoryOpenHelper.insertCategory(db,
                        "Transportation");
                categoryOpenHelper.insertCategory(db,
                        "Utilities");
                categoryOpenHelper.insertDataSet(db, "Business and Economy", "Business Licenses (Active - Resident)",
                        "<h1>Business Licenses (Active - Resident)</h1> <hr /> <h2>About</h2> <p><p>New Westminster has an annual renewal of approximately 4,000 business licenses each year. Business Licensing also issues licenses for liquor establishments and municipal decals.</p> <p>Things to know</p> <p>1) Before you sign a lease, it\\'s important for prospective business owners who are applying for business licenses to check with the Building, Planning and Licensing divisions on property they wish to lease or buy in regards to outstanding orders or issues pertaining to that property.</p> <p>2) Before you sign a lease, check with the Planning and Building Department to make sure your business is a permitted use on the site.</p> <p>3) Before you purchase a sign for your business, review the requirements of the sign bylaw with the Planning Division. Click here for Sign Permit information.</p> <p>4) Each space in a building has its own specific approved use and sometimes the use of that space cannot be changed without approval and/or permit.</p> <p><a href=\"https://www.newwestcity.ca/business_licences.php\">https://www.newwestcity.ca/business_licences.php</a></p></p>\n"+
                        "\n");
                categoryOpenHelper.insertDataSet(db, "Business and Economy", "Business Licenses (Inter-Municipal)", "<h1>Business Licenses (Inter-Municipal) </h1> <hr /> <h2>About</h2> <p><p>As of October 1, 2013, an Inter-municipal Business License will be available in the Metro West region. For $250, eligible businesses may be licensed to work in all of the following municipalities: City of New Westminster, City of Burnaby, Corporation of Delta, City of Richmond, City of Surrey, and City of Vancouver. </p> <p>Eligibility is limited to inter-municipal businesses, defined as trades contractors or other professionals (related to the construction industry) that provide a service or product other than from their fixed and permanent location. Only eligible businesses which have fixed and permanent location in one of the participating municipalities are eligible for the IMBL.</p> <p>For further information, please contact the City of New Westminster Business Licensing Office at 604-527-4565.</p> <p><a href=\"https://www.newwestcity.ca/business_licences.php\">https://www.newwestcity.ca/business_licences.php</a></p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Business and Economy", "Business Licenses (New for 2017)", "<h1>Business Licenses (New for 2017)</h1> <hr /> <h2>About</h2> <p><p>Every business in the City of New Westminster is required to have a valid business license before beginning operation. This includes home-based businesses, commercial and industrial operations and owners of apartment rental properties.</p> <p><a href=\"http://www.newwestcity.ca/business/permits_licenses/business_licences.php\">http://www.newwestcity.ca/business/permits_licenses/business_licences.php</a></p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Business and Economy", "Business Licenses (Non-Residents)", "<h1>Business Licenses (Non-Residents)</h1> <hr /> <h2>About</h2> <p><p>Contractors from different municipalities doing business within New Westminster</p> <p><a href=\"https://www.newwestcity.ca/business_licences.php\">https://www.newwestcity.ca/business_licences.php</a></p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Business and Economy", "Demographic - Detailed Age Profile (Census 2011)", "<h1>Demographic - Detailed Age Profile (Census 2011)</h1> <hr /> <h2>About</h2> <p><p>Census 2011 information summarized by ages (by individual years of age and age groupings) and gender, neighborhoods, census tracts and dissemination areas. Also contains descriptive information about the data source files and notes about the use of the data.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Business and Economy", "Demographic Profiles (Census 1986,1991,1996,2001,2006)", "<h1>Demographic Profiles (Census 1986,1991,1996,2001,2006)</h1> <hr /> <h2>About</h2> <p><p>Information for the City of New Westminster from the 1986, 1991, 1996, 2001 and 2006 Censuses.</p> <p>This information includes age, housing characteristics, immigration, ethnicity, labour force, population change, income, education, household type, language information etc. Also contains descriptive information about the data source files and notes about the use of the data.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Business and Economy", "Demographic Profiles (Census 2011)", "<h1>Demographic Profiles (Census 2011)</h1> <hr /> <h2>About</h2> <p><p>2011 census information summarized by </p> <p>a) city b) neighborhoods c) census tracts and d) dissemination areas. </p> <p>Census information includes age, household type, family type, mother tongue, home language, type of dwelling etc. Also contains descriptive information about the data source files and notes about the use of the data.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Business and Economy", "Demographic Profiles (National Household Survey 2011)", "<h1>Demographic Profiles (National Household Survey 2011)</h1> <hr /> <h2>About</h2> <p><p>Contains tabs with 2011 National Household Survey information for the City of New Westminster, New Westminster neighbourhoods and New Westminster census tracts. National Household Survey includes information on income, housing characteristics, ethnicity, immigration, education, labour force etc. Also contains descriptive information about the data source files and notes about the use of the data.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Business and Economy", "Sidewalk Café Location (Sidewalk Encroachment Agreements)", "<h1>Sidewalk Caf&eacute; Location (Sidewalk Encroachment Agreements)</h1> <hr /> <h2>About</h2> <p><p>List of the locations of all Sidewalk Encroachment Agreements</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Business and Economy", "Workforce - New West Resident Commuting Patterns (NHS 2011)", "<h1>Workforce - New West Resident Commuting Patterns (NHS 2011)</h1> <hr /> <h2>About</h2> <p><p>This file contains information on the commuting patterns of workers who live in New Westminster (regardless of which municipality their place of work is located in). The information is from the 2011 National Household Survey and contains mode of transportation, time leaving for work, commute duration and commuting destinations. Also contains descriptive information about the data source files and notes about the use of the data.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Business and Economy", "Workforce - New West Worker Commuting Patterns (NHS 2011)", "<h1>Workforce - New West Worker Commuting Patterns (NHS 2011)</h1> <hr /> <h2>About</h2> <p><p>Contains information on workers who work within the boundaries of the City of New Westminster (regardless of their municipality of residence).</p> <p>Information is from the 2011 National Household Survey and includes mode of transportation, time arriving at work, commute duration and commuting origin. Also contains descriptive information about the data source files and notes about the use of the data.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Business and Economy", "Workforce Profile (NHS 2011)", "<h1>Workforce Profile (NHS 2011)</h1> <hr /> <h2>About</h2> <p><p>2011 National Household Survey information on workers who work in New Westminster (regardless of municipality of residence). Information includes occupation, industry, employment income (before-tax), work activity, age and sex and education. Also contains descriptive information about the data source files and notes about the use of the data. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "City Government", "Addresses", "<h1>Addresses</h1> <hr /> <h2>About</h2> <p><p>A list of addresses for the City of New Westminster.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "City Government", "City Boundaries", "<h1>City Boundaries</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>City of New Westminster Boundaries. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "City Government", "City Owned Property", "<h1>City Owned Property</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Parcels of property currently owned by the Corporation of the City of New Westminster. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "City Government", "Councillor Contact Information", "<h1>Councillor Contact Information</h1> <hr /> <h2>About</h2> <p><p>The City of New Westminster wants to facilitate residents and the general public access to the elected officials of the City. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "City Government", "Election Results 1990 - Present", "<h1>Election Results 1990 - Present</h1> <hr /> <h2>About</h2> <p><p>The spreadsheet provides the candidates, voting locations and the results for the candidate both total and by location for each election from 1990 forward.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "City Government", "Neighbourhoods Boundaries", "<h1>Neighbourhoods Boundaries</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>These new boundaries are used for contemporary planning functions and are the basis for most of the statistics used in recent Census data published by the City of New Westminster. The boundaries for all neighbourhoods except for Downtown, North Arm North and Queens Park are the same for the redefined neighbourhood boundaries as in the original neighbourhood boundaries. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "City Government", "Number of City Employees", "<h1>Number of City Employees</h1> <hr /> <h2>About</h2> <p><p>Financial Disclosure form completed annual by all elected officials. Number of city employees by year.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Community", "Cemeteries", "<h1>Cemeteries</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Community", "City Facility Sites", "<h1>City Facility Sites</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Community", "Community Service Assets", "<h1>Community Service Assets</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>A listing of community services and supports. More specifically, it includes information on emergency, transitional and supportive housing; transition and second stage housing for women; addiction and mental health services; drop-in and meal programs; education and job training opportunities; and government services.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Community", "Public Art", "<h1>Public Art</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Community", "School Buildings", "<h1>School Buildings</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Community", "School Catchment Boundaries", "<h1>School Catchment Boundaries</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Primary, Middle and Secondary School Boundaries.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Community", "School Sites", "<h1>School Sites</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Electrical", "City Energy Use Through Time", "<h1>City Energy Use Through Time</h1> <hr /> <h2>About</h2> <p><p>Financial disclosure form completed annually by all elected officials. Shows the amount of energy consumed and greenhouse gases created through time.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Environment", "Riparian", "<h1>Riparian</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Finance", "Grants (Awarded for 2016)", " <h1>Grants (Awarded for 2016)</h1>\n" +
                        "\t<hr />\n" +
                        "\n" +
                        "\t\t\t<h2>About</h2>\n" +
                        "\n" +
                        "<p><p>A spreadsheet and accompanying documents listing grants awarded for year 2016.</p>\n" +
                        "<p>Grant Categories;</p>\n" +
                        "<p>1) Festival Event Grants 2) Heritage Grants 3) Environmental Grants 4) Community Grants 5) Arts and Culture Grants 6) Child Care Grants 6) City Partnership Grants 7) Amateur Sports Grants</p>\n" +
                        "<p>For more information go to <a href=\"http://www.newwestcity.ca/business/grants/community_grants.php\">City Grants Page</a></p>\n" +
                        "<p>Supporting documents;</p>\n" +
                        "<p><a href=\"http://opendata.newwestcity.ca/downloads/GrantDocuments/Awarded_2016_City_Grants.xlsx\">City Grants Summary Sheet</a></p>\n" +
                        "<p><a href=\"http://opendata.newwestcity.ca/downloads/GrantDocuments/2016_City_Partnership_Grants.zip\">2016 City Partnership Grants.zip</a></p>\n" +
                        "<p><a href=\"http://opendata.newwestcity.ca/downloads/GrantDocuments/2016_Festival_Grants.zip\">2016 Festival Grants.zip</a></p>\n" +
                        "<p><a href=\"http://opendata.newwestcity.ca/downloads/GrantDocuments/2016_Community_Grants.zip\">2016 Community Grants.zip</a></p>\n" +
                        "<p><a href=\"http://opendata.newwestcity.ca/downloads/GrantDocuments/2016_Arts_and_Culture_Grants.zip\">2016 Arts and Culture Grants.zip</a></p>\n" +
                        "<p><a href=\"http://opendata.newwestcity.ca/downloads/GrantDocuments/2016_Amateur_Sport_Grants.zip\">2016 Amateur Sport Grants.zip</a></p>\n" +
                        "<p><a href=\"http://opendata.newwestcity.ca/downloads/GrantDocuments/2016_Heritage_Grants.zip\">2016 Heritage Grants.zip</a></p>\n" +
                        "<p><a href=\"http://opendata.newwestcity.ca/downloads/GrantDocuments/2016_Child_Care_Grant_Program.zip\">2016 Child Care Grant Program.zip</a></p>\n" +
                        "<p><a href=\"http://opendata.newwestcity.ca/downloads/GrantDocuments/2016_Environmental_Grants.zip\">2016 Environmental Grants.zip</a></p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Finance", "Schedule of Goods and Services (2015)", "<h1>Schedule of Goods and Services (2015)</h1>\n" +
                        "\t<hr />\n" +
                        "\n" +
                        "\t\t\t<h2>About</h2>\n" +
                        "\n" +
                        "<p><p>Excel listing of all suppliers and service provides in the report period, plus the amount paid</p></p>\n" +
                        "]]>");
                categoryOpenHelper.insertDataSet(db, "Finance", "Statement of Financial Information (2015)", "<h1>Statement of Financial Information (2015)</h1>\n" +
                        "\t<hr />\n" +
                        "\n" +
                        "\t\t\t<h2>About</h2>\n" +
                        "\n" +
                        "<p><p>Remuneration of City Employees &amp; Council Members</p></p>");
                categoryOpenHelper.insertDataSet(db, "Heritage", "Building Age", "<h1>Building Age</h1> <hr /> <h2>About</h2> <p><p>The age of most buildings in the City (year it was built) as well as some historical data such as the Building Name, Developer/Builder, Architect/Designer and year the building has been moved if relevant and available. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Heritage", "Heritage Register", "<h1>Heritage Register</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Official listing of properties deemed to have heritage value. </p> <p>Visit the <a href=\"http://arcgis.newwestcity.ca/RegisterDesignation/index.html\">Heritage Register Website Viewer</a>.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Heritage", "Heritage Resource Inventory", "<h1>Heritage Resource Inventory</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Complete unofficial listing of properties deemed to have heritage value, demolished and standing buildings. Visit the <a href=\"http://www.arcgis.com/apps/MapSeries/index.html?appid=50c6b97e69dc4afd84ce30f4f5d50571/\">Heritage Resource Inventory Website Viewer</a>!</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Block Reference File", "<h1>Block Reference File</h1> <hr /> <h2>About</h2> <p><p>The blocks correspond to a division of the City into about 400 blocks, set up by the City Planner in about 1970. The purpose of these geographic descriptions was to enable more rapid tallying of information by subareas of the City. The geographic subdivision keys would provide easier selection of which properties to include in a run for a report without having to rely on property folio designations which are subject to change, consolidation and subdivision.</p> <p>Block reference file used with &quot;Historical Development Statistics&quot; and &quot;Landuse Percentages by Block&quot; datasets</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Building Attributes", "<h1>Building Attributes</h1> <hr /> <h2>About</h2> <p><p>Building development specifics including the number of floors above and below ground, the number of residential units, square footage, size of the footprint and site coverage, and address. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Building Footprints", "<h1>Building Attributes</h1> <hr /> <h2>About</h2> <p><p>Building development specifics including the number of floors above and below ground, the number of residential units, square footage, size of the footprint and site coverage, and address. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Community Conversation on Housing Comments (Our City 2014)", "<h1>Community Conversation on Housing Comments (Our City 2014)</h1> <hr /> <h2>About</h2> <p><p>Got to <a href=\"https://www.newwestcity.ca/ourcity\">https://www.newwestcity.ca/ourcity</a> for more details</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Contours", "<h1>Contours</h1> <hr /> <h2>About</h2> <p><p>1 meter intervals</p></p>");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Historical Development Statistics", "<h1>Historical Development Statistics </h1> <hr /> <h2>About</h2> <p><p>Statistics per Hectare show a sample of four types of uses or content in a particular block/area; the number of residences, the number of buildings, the floor space ratio which is the ratio of a buildings total floor area to the size of the land upon which it is built, and the number of parking spaces on property in the area. </p> <p>Use in conjunction with the Block Reference Dataset.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Land Use", "<h1>Land Use</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Land use represents what a parcel of land is currently being used for (i.e., the land parcel\\'s primary use). The land use shown in this layer does not necessarily reflect the zoning or the OCP designation of the land.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Land Use Industrial", "<h1>Land Use Industrial</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Land use represents what a parcel of land is currently being used for (i.e., the land parcel\\'s primary use). The land use shown in this layer does not necessarily reflect the zoning or the OCP designation of the land.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Landuse Percentages by block", "<h1>Landuse Percentages by block </h1> <hr /> <h2>About</h2> <p><p>The Landuse Percentages show what proportion of a block/area has a particular use (e.g., being used for Commercial purposes or Single Family Residences). </p> <p>Use in conjunction with the Block Reference Dataset</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Orthophotography", "<h1>Orthophotography</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Aerial photography over the City of New Westminster</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Parcel Blocks", "<h1>Parcel Blocks</h1> <hr /> <h2>About</h2> <p><p>Block outlines of contiguous aggregated parcels. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Parcels", "<h1>Parcels</h1> <hr /> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Projects on the Go", "<h1>Projects on the Go</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Current applications for rezoning, development permit and heritage revitalization agreement projects currently being processed by the City, including application status, architect/ developer information, and staff contact.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Survey Monuments", "<h1>Survey Monuments</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>For more information see <a href=\"http://a100.gov.bc.ca/pub/mascotw/public/index.html\">Mascot at GeoBC</a> Reference Systems and Survey Monuments.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Lands and Development", "Zoning", "<h1>Zoning</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>The City of New Westminster Zoning Bylaw No. 6680 was adopted by Council September 17, 2001. Subsequent amendments to the Zoning Bylaw are consolidated for convenience only. For accurate interpretation, the public is encouraged to consult the Official Zoning Bylaw (including maps and amendments) available for viewing at City Hall in the Planning Division or Legislative Services Department.</p> <p>For more information: <a href=\"http://www.newwestcity.ca/city_hall/bylaws/articles250.php\">http://www.newwestcity.ca/city_hall/bylaws/articles250.php</a></p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Accessible Public Washrooms", "<h1>Accessible Public Washrooms</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Listing of all the accessible washrooms that are available within the City.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Bike Routes", "\t<h1>Bike Routes</h1>\n" +
                        "\t<hr />\n" +
                        "\n" +
                        "\t\t\t<div class=\"row\">\n" +
                        "\t\t\t<div class=\"col-sm-6\">\n" +
                        "\t\t\t\t<h2>About</h2>\n" +
                        "\n" +
                        "<p><p>This dataset contains bike routes including planned and current bikeways, on-street and off-street, as well as dedicated lanes. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Drinking Fountains", "<h1>Drinking Fountains</h1>\n" +
                        "\t<hr />\n" +
                        "\n" +
                        "\t\t\t<div class=\"row\">\n" +
                        "\t\t\t<div class=\"col-sm-6\">\n" +
                        "\t\t\t\t<h2>About</h2>\n" +
                        "\n" +
                        "<p>No Description Available</p>");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Greenways", "<h1>Greenways</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Greenways provide important cycling routes across the City connecting from one community to another and between major parks.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Off Leash Dog Areas", "<h1>Off Leash Dog Areas</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Park Benches and Dedications", "<h1>Park Benches and Dedications</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Park benches locations throughout the city.</p> <p>Green: Undedicated benches Purple : Dedicated benches</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Park Buildings", "<h1>Park Buildings</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Various buildings located on park property and used for multiple purposes. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Park Greenspaces", "<h1>Park Greenspaces</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Park Structures", "<h1>Park Structures</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Park Structure data will include the structure name, structure type, quantity, monument dedication inscription, furnishing photo graph (as available) park name and/or location. The types of park structures included in the collection are • Bleachers • drinking fountains • Park Lighting • Monuments • Basketball hoops • Horticultural Planting Areas • Picnic Tables</p> <p>Monument dedication inscriptions were provided to the City for publically displayed on the monument, and as such the City has approval for the release. The inscription includes name, potentially a date range and inscription. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Park Trails", "<h1>Park Trails</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Parks", "<h1>Parks</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Playgrounds", "<h1>Playgrounds</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Includes the playgrounds within the parks in the City and the types of equipment available for play. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Sports Fields", "<h1>Sports Fields</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Sports Fields will include various types of activity fields including those used for softball, soccer, rugby, football, and lacrosse. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Tree Inventory - East", "<h1>Tree Inventory - East</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Street trees do more than beautify our City and create community pride. Street trees have been scientifically proven to: save energy by reduce heating or cooling costs for buildings they cover calm traffic clean air filter dust absorb sound cool area under tree and absorb humidity provide habitats for birds, mammals and insects</p> <p>Parks staff select street trees based on established research to ensure the trees are viable without damaging public or private property and are resilient to disease and pests. Street trees generally have the following characteristics: Small to medium size (20 - 35 feet at maturity) Raised crown to provide maximum clearance between the sidewalk and lower branches Non-aggressive root systems Growth habits that are compatible with streetscapes (i.e. trees with upright growth habits are used in narrow spaces) Ornamental characteristics (i.e. bark, fall colour, flowers, etc.) that enhance the neighbourhood.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Parks and Recreation", "Tree Inventory - West", "<h1>Tree Inventory - West</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Street trees do more than beautify our City and create community pride. Street trees have been scientifically proven to: save energy by reduce heating or cooling costs for buildings they cover calm traffic clean air filter dust absorb sound cool area under tree and absorb humidity provide habitats for birds, mammals and insects</p> <p>Parks staff select street trees based on established research to ensure the trees are viable without damaging public or private property and are resilient to disease and pests. Street trees generally have the following characteristics: Small to medium size (20 - 35 feet at maturity) Raised crown to provide maximum clearance between the sidewalk and lower branches Non-aggressive root systems Growth habits that are compatible with streetscapes (i.e. trees with upright growth habits are used in narrow spaces) Ornamental characteristics (i.e. bark, fall colour, flowers, etc.) that enhance the neighbourhood.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Public Safety", "Emergency Incidents By Fire Hall", "<h1>Emergency Incidents By Fire Hall</h1> <hr /> <h2>About</h2> <p><p>Emergency incident types by year by hall. The total of yearly calls is also represented.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Public Safety", "Emergency Incidents By Fire Hall Summary", "<h1>Emergency Incidents By Fire Hall Summary</h1> <hr /> <h2>About</h2> <p><p>Emergency incident summary counts for past five years by hall. The total monthly and yearly calls are represented.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Public Safety", "Emergency Incidents by Type (Fire and Rescue Services)", "<h1>Emergency Incidents by Type (Fire and Rescue Services)</h1> <hr /> <h2>About</h2> <p><p>Incident types by month/year. The total and percentage of incidents of total calls is also represented. Only the current year is available in .csv.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Public Safety", "Fire and Rescue Services Buildings", "<h1>Fire and Rescue Services Buildings</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Public Safety", "Fire Incidents by Year", "<h1>Fire Incidents by Year</h1> <hr /> <h2>About</h2> <p><p>Fire incidents by year. The total number of incidents is broken down into reportable to the Office of the Fire Commissioner and non reportable.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Public Safety", "Hospital Buildings", "<h1>Hospital Buildings</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Public Safety", "Oil Tanks (Removed/Decommissioned)", "<h1>Oil Tanks (Removed/Decommissioned)</h1> <hr /> <h2>About</h2> <p><p>The number of underground storage tanks that are active, removed, or outstanding in the removal process, by year.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Public Safety", "Police Buildings", "<h1>Police Buildings</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Alternative Fuels and Electric Charging Stations", "<h1>Alternative Fuels and Electric Charging Stations</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Electric vehicles are an environmentally friendly mode of transportation. As cleaner emission vehicles gain momentum across the lower mainland, the City of New Westminster is putting itself on the map alongside leading municipalities by incorporating electric vehicles into the City\\'s automotive fleet and installing electric vehicle charging stations in the community.</p> <p>For a map of EV charging stations and their availability, visit <a href=\"https://goelectricstations.com/map-charging-stations.html\">Go Electric Stations</a>.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Bike Routes", "<h1>Bike Routes</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>This dataset contains bike routes including planned and current bikeways, on-street and off-street, as well as dedicated lanes. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Bus Routes", "<h1>Bus Routes</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Bus Routes within New Westminster. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Bus Stops", "<h1>Bus Stops</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Bus Stop locations within New Westminster noting which are accessible. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "ICBC Crash Data", "<h1>ICBC Crash Data</h1> <hr /> <h2>About</h2> <p><p>Lower mainland crashes</p> <p>See how many crashes are happening at intersections in New Westminster and around the Lower Mainland. </p> <p>Click <a href=\"http://www.icbc.com/about-icbc/newsroom/Pages/Lower-Mainland-Crash-Map.aspx\">here</a> to visit the ICBC Lower Mainland Crash website page!</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Intersections", "<h1>Intersections</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>The junctions at-grade of two or more roads either meeting or crossing. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Parking Pay Stations", "<h1>Parking Pay Stations</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Identifies the locations of all multi-space digital pay stations for parking in the City. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Railways", "<h1>Railways</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Regulatory Signs", "<h1>Regulatory Signs</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Regulatory street signs which also include Do Not Enter and 3 and 4 Way tabs. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "School Walking Routes", "<h1>School Walking Routes</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Walking to school promotes healthy and safe communities benefiting children, families, and the earth.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "SkyTrain Centreline", "<h1>SkyTrain Centreline</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "SkyTrain Stations", "<h1>SkyTrain Stations</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>SkyTrain Stations within New Westminster shown as footprints of the structure shapes. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "SkyTrain Stations Points", "<h1>SkyTrain Stations Points</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>SkyTrain Stations within New Westminster shown as point locations. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Speed Signs", "<h1>Speed Signs</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>These street signs include Speed Signs and Speed Tabs. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Street Features", "<h1>Street Features</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Street Names", "<h1>Street Names</h1> <hr /> <h2>About</h2> <p><p>List of all current in-use street names used within the City. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Street Network", "<h1>Street Network</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Street centerlines and road classification</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Traffic Controllers/Signals", "<h1>Traffic Controllers/Signals</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Traffic Volumes", "<h1>Traffic Volumes</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Traffic volume counts at midblock points between the years 2006 and 2016 inclusive. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Truck Routes", "<h1>Truck Routes</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Warning Signs", "<h1>Warning Signs</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>Warning Street Signs include Object Markers, Playground Ahead, Speed Bumps, Neighbourhood Speed Humps, No Exits, School Areas, and Misc Warning Signs. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Webcam Links", "<h1>Webcam Links</h1> <hr /> <h2>About</h2> <p><p>Active webcam locations in New Westminster. </p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Transportation", "Wheelchair Ramps", "<h1>Wheelchair Ramps</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Utilities", "Drinking Fountains", "<h1>Drinking Fountains</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Utilities", "Sewer Catchbasins", "<h1>Sewer Catchbasins</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Utilities", "Sewer Culverts", "<h1>Sewer Culverts</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Utilities", "Sewer Ditches", "<h1>Sewer Ditches</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Utilities", "Sewer Mains", "<h1>Sewer Mains</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Utilities", "Sewer Maintenance Holes", "<h1>Sewer Maintenance Holes</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p>No Description Available</p>\n");
                categoryOpenHelper.insertDataSet(db, "Utilities", "Water Hydrants", "<h1>Water Hydrants</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>A hydrant is an outlet from a fluid main often consisting of an upright pipe with a valve attached from which fluid (e.g. water or fuel) can be tapped.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Utilities", "Water Pressure Zones", "<h1>Water Pressure Zones</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>This polygon feature class represents each water pressure zone in the City of New Westminster water distribution system. The data was developed to represent the location of water pressure zones for the purpose of mapping, analysis, planning and maintenance of utilities. The accuracy of this data varies and should not be used for precise measurements or calculations.</p> <p></p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Utilities", "Water Quality Data", "<h1>Water Quality Data</h1> <hr /> <h2>About</h2> <p><p>A hydrant is an outlet from a fluid main often consisting of an upright pipe with a valve attached from which fluid (e.g. water or fuel) can be tapped. This data set presents the raw data from which our Annual Water Quality report is generated. For full context for the data please refer to the report.</p> <p>NWR Comp 2015.xlsm - Monthly bacteriological analysis of portable water samples</p> <p>NWR Numbers 2015.xlsm - Monthly samples for coliform bacteria</p> <p>NWR By-station 2015.xlsm - Full year water quality testing by station (addresses given are locations of the sampling station)</p> <p>NWR HPC 2015.xlsm - Monthly heterotrophic plate count</p> <p>NWR 4Q DBP.xlsm - 4th quarter disinfectant by product reports</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Utilities", "Water Valves", "<h1>Water Valves</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>A device that regulates the flow of water.</p></p>\n");
                categoryOpenHelper.insertDataSet(db, "Utilities", "Watermains", "<h1>Watermains</h1> <hr /> <div class=\"row\"> <div class=\"col-sm-6\"> <h2>About</h2> <p><p>A principal pipe in a system of pipes for conveying water, especially one installed underground.</p></p>\n");



                db.setTransactionSuccessful();
            }
            finally
            {
                db.endTransaction();
            }
        }

        db.close();
    }

    private class CategoryLoaderCallbacks
            implements LoaderManager.LoaderCallbacks<Cursor>
    {
        @Override
        public Loader<Cursor> onCreateLoader(final int    id,
                                             final Bundle args)
        {
            final Uri uri;
            final CursorLoader loader;

            uri    = CategoryContentProvider.CONTENT_URI;
            loader = new CursorLoader(MainActivity.this, uri, null, null, null, null);

            return (loader);
        }

        @Override
        public void onLoadFinished(final Loader<Cursor> loader,
                                   final Cursor         data)
        {
            adapter.swapCursor(data);
        }

        @Override
        public void onLoaderReset(final Loader<Cursor> loader)
        {
            adapter.swapCursor(null);
        }
    }
}
